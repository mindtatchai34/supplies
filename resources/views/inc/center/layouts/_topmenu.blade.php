<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
    <svg class="c-icon c-icon-lg">
        <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-menu"></use>
    </svg>
</button>

<a class="c-header-brand d-lg-none" href="#">
    <svg width="118" height="46" alt="CoreUI Logo">
        <use xlink:href="assets/brand/coreui.svg#full"></use>
    </svg>
</a>

<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
    <i class="fas fa-bars"></i>
</button>

<ul class="c-header-nav d-md-down-none">
    {{-- <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Dashboard</a></li>
    <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Users</a></li>
    <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Settings</a></li> --}}
</ul>
<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        @yield('breadcrumb')
    </ol>
</div>
