@extends('Layouts.main')
@section('contents')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <b>เพิ่มสินค้า</b>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.index') }}" class="btn btn-info">ย้อนกลับ</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <form class="mt-3" method="POST" action="{{ url('admin') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlFile1">รูปภาพสินค้า</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">เลขคำสั่งซื้อ</label>
                            <input type="text" class="form-control" name="orderCode">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">ชื่อสินค้า</label>
                            <input type="text" class="form-control" name="productName">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">ที่อยู่จัดส่ง</label>
                            <textarea class="form-control" rows="3" name="address"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">สถานะสินค้า</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="statusId">
                                @foreach($status as $statuses)
                                    <option value="{{ $statuses->id }}">{{ $statuses->statusName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <strong>ข้อมูลผู้สั่งซื้อ</strong>
                        <div class="form-group mt-3">
                            <label for="exampleFormControlInput1">ชื่อลูกค้า</label>
                            <input type="text" class="form-control" placeholder="คุณ..." name="cusName">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">ชื่อ Facebook</label>
                            <input type="text" class="form-control" placeholder="FaceBook Name" name="fbName">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Facebook Link</label>
                            <input type="text" class="form-control" placeholder="www.facebook.com" name="fbLink">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary mb-2" style="float: right;" value="บันทึกข้อมูล">
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection