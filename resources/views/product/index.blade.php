@extends('Layouts.main')
@section('contents')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card pr-3 pl-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <b>ORDER ทั้งหมด</b>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.create') }}" class="btn btn-info">เพิ่ม Order</a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">เลขที่สั่งซื้อ</th>
                        <th scope="col">รูปภาพ</th>
                        <th scope="col">ชื่อสินค้า</th>
                        <th scope="col">Facebook ลูกค้า</th>
                        <th scope="col">สถานะสินค้า</th>
                        <th scope="col">วันที่เพิ่มข้อมูล</th>
                        <th scope="col">จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product as $products)
                        <tr>
                            <td>{{ $products->orderCode }}</td>
                            <td>{{ $products->imgName }}</td>
                            <td>{{ $products->name }}</td>
                            <td><a href="{{ $products->fbLink }}">{{ $products->fbName }}</a></td>
                            <td><span class="{{ $products->status->class }}">{{ $products->status->statusName }}</span>
                            <td><span class="badge bg-secondary">{{ formatDateThat($products->createAt) }}</span></td>
                            <td>
                                <a href="{{route('admin.edit',$products->id)}}" class="btn btn-warning">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="{{ route('admin.destroy',$products->id) }}" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        {{ $product->links() }}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection