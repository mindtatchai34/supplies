<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Auth')->prefix('auth')->name('auth.')->group(function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login')->middleware('guest');
    Route::post('/login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');
});
Route::middleware('auth')->namespace('Admin')->prefix('admin')->name('admin.')->group(function () {

    Route::resource('/', 'ProductController');

});
